<?php
include('includes/header.php'); 
include('includes/navbar.php'); 
include('includes/DBconnection.php');
?>

<?php
$id = $_GET["doctor_id"];


$sql = "SELECT name from employee WHERE ID = '$id'";

$result = $conn->query($sql);
$doctorName ='';
while($row = $result->fetch_assoc()) {
   $doctorName = $row['name'];
 }

$sql = "SELECT * from doctoravailability WHERE ID = '$id'";
$result = $conn->query($sql);


$doctorAvailability =array();
while($row = $result->fetch_assoc()) {
   array_push($doctorAvailability, $row);
 }
 $dayOfWeek = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday','Friday','Saturday');


?>
<div class="container-fluid">
	<a class="btn btn-primary" href='doctors.php' style="margin: 10px;width: 5%">Back</a>
   <div class="card shadow mb-4">
  <div class="card-header py-3">
    <h4 class="m-0 font-weight-bold text-primary">Doctor availability: <?php  echo " " . $doctorName  ?>
    </h4>
  </div>

  <div class="card-body">

    <div class="table-responsive">

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    
        <thead style="background-color: #cdd0d8; color: white; font-size: 25px">
          <tr>
            <th> Day </th>
            <th> from </th>
            <th> to </th>
         
          </tr>
        </thead>

        <tbody style="color: black; font-size: 20px">

          <?php
          	foreach ($dayOfWeek as $value) {
          		echo "<tr>";
          		echo "<td>" .$value. "</td>";
          		$start_time = '-';
          		$end_time = '-';
          		foreach ($doctorAvailability as $row){
          			if($row['day'] == $value){
          				$start_time = date('H:i',strtotime($row['start_time'] ));
          				$end_time = date('H:i',strtotime($row['end_time']));
          			}
          		}
          		echo "<td>" .$start_time. "</td>";

          		echo "<td>" .$end_time. "</td>";

          		echo "</tr>";
          	}
            
          ?>

        </tbody>
      </table>

    </div>
  </div>
</div>

</div>






 <?php
include('includes/footer.php');
?>