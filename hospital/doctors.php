 <?php
include('includes/header.php'); 
include('includes/navbar.php'); 
include('includes/DBconnection.php');
?>
<?php

$sql = "SELECT E.ID, E.name, E.date_of_birth, E.contact_number, E.annual_salary, D.specialization FROM employee E INNER JOIN doctors D WHERE E.ID = D.ID";
$result = $conn->query($sql);


$doctorDetails =  array();
 while($row = $result->fetch_assoc()) {
  array_push($doctorDetails, $row);
 }

?>

<div class="container-fluid">
   <div class="card shadow mb-4">
  <div class="card-header py-3">
    <h4 class="m-0 font-weight-bold text-primary">Doctor Data </h4>
  </div>

  <div class="card-body">

    <div class="table-responsive">

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    
        <thead style="background-color: #cdd0d8; color: white; font-size: 20px">
          <tr>
            <th> Doctor ID </th>
            <th> Name </th>
            <th> Date of Birth </th>
            <th>Contact Number</th>
            <th>Annual Salary</th>
            <th>Specialization</th>
            <th>Availability</th>
        
          </tr>
        </thead>

        <tbody style="color: black; font-size: 17px">
          <?php
             foreach ($doctorDetails as $row) {
                echo "<tr>";
                echo "<td>" . $row["ID"] ."</td>";
                echo "<td>" . $row["name"] . "</td>";
                echo "<td>" . $row["date_of_birth"] . "</td>";
                echo "<td>" . $row["contact_number"] . "</td>";
                echo "<td>" . $row["annual_salary"] . "</td>";
                echo "<td>" . $row["specialization"] . "</td>";
                echo  "<td> <a    href = \"doctorAvailability.php?doctor_id=" . $row["ID"]. "\" class=\"btn btn-primary\"> Show Availability of Doctor </a> </td>";
               
             }
          ?>

        </tbody>
      </table>

    </div>
  </div>
</div>

</div>

 <?php
include('includes/footer.php');
?>