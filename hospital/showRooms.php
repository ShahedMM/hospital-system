<?php
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<?php
$conn = mysqli_connect("localhost","admin","admin","hospitaldb");
if ($conn->connect_error) {
    echo "Connection failed: " . $conn->connect_error;
}


$roomList = array();
$sql = 'SELECT * FROM rooms ORDER BY cost_per_day ASC';
$result = $conn->query($sql);

while($row = $result->fetch_assoc()){
 	array_push($roomList, $row);
}


?>

<div class="container">
	<div class="row  ">
		<?php
			foreach ($roomList as $value) {
				$room_no = $value['room_no'];
				$type = $value['type'];
				$status = ($value['status'])=='0' ? 'Available' : 'Taken' ;
				$price = $value['cost_per_day'];

				$img = $value['image_url'];

				echo "<div class ='col-4' style='width: 15%;'>
				<div class='card' style='margin: 10px;padding: 10px'>
  						<img class='card-img-top' src=".$img." alt='Card image cap'>
  						<div class='card-body'>
  						<h5 class='card-title'>Room Number: $room_no</h5>
  						</div>
  						 <ul class='list-group list-group-flush'>
    					<li class='list-group-item' ><span style='font-weight:bold'>Status:</span> $status</li>
    					<li class='list-group-item' ><span style='font-weight:bold'>Type: </span> $type</li>
    					<li class='list-group-item' ><span style='font-weight:bold'>Price per night: </span>\$$price</li>
    					</ul>
    					</div>
    					</div>";

			}



		?>

</div>
</div>
<?php
include('includes/footer.php');
?>