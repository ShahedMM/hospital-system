   <!-- Sidebar -->
   <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
  <div class="sidebar-brand-icon">
    <i class="fas fa-hospital"></i>
  </div>
  <div class="sidebar-brand-text mx-3">Admin panel</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">


<!-- Divider -->
<hr class="sidebar-divider">


<li class="nav-item">
  <a class="nav-link" href="patients.php">
    <i class="fas fa-procedures"></i>
    <span>Patients</span></a>
</li>


<li class="nav-item">
  <a class="nav-link" href="doctors.php">
    <i class="fas fa-fw fa-user-md"></i>
    <span>Doctors</span></a>
</li>

<!-- <li class="nav-item">
  <a class="nav-link" href="addEntry.php">
    <i class="fas fa-fw fa-briefcase-medical"></i>
    <span>Add Entry</span></a>
</li>
 -->

<!-- Nav Item - Charts -->
<li class="nav-item">
  <a class="nav-link" href="showRooms.php">
    <i class="fas fa-fw fa-hospital-alt"></i>
    <span>Rooms</span></a>
</li>



<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">


</ul>
<!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

        

          <!-- Topbar Search -->
         <!-- <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form> -->


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Date and Time -->
            <li class="nav-item ">
               <?php echo date("l") . ", " . date("d/m/Y") . " " . date("h:i a"); ?>
            </li>
          </ul>

        </nav>
        <!-- End of Topbar -->


