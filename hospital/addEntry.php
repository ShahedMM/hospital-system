<?php
include('includes/header.php'); 
include('includes/navbar.php'); 
?>

<?php

$conn = mysqli_connect("localhost","admin","admin","hospitaldb");
if ($conn->connect_error) {
    echo "Connection failed: " . $conn->connect_error;
}

$patient_no = $_GET['patient_no'];
$sql = "SELECT name FROM patient WHERE patient_no= '$patient_no'";
$result = $conn->query($sql);
$patient_name ='';
 while($row = $result->fetch_assoc()){
 	$patient_name = $row['name'];
 }
 $sql = "SELECT DISTINCT specialization FROM doctors";
 $specialization = $conn->query($sql);

$sql = "SELECT * FROM `rooms` WHERE status = 0";
$availableRooms = $conn->query($sql);

?>

<div class="container-fluid">
   <div class="card shadow mb-4">
  <div class="card-header py-3">
    <h4 class="m-0 font-weight-bold text-primary">Add New Entry </h4>
  </div>

  <div class="card-body">
<form action="chooseDoctor.php" method="POST">

        <div class="modal-body">
        
                <input style="display: none" type="text" name="patient_no" class="form-control" placeholder="Enter patient name" value=<?php echo $patient_no?> >
           
            <div class="form-group">
                <label> Name </label>
                <input type="text" name="name" class="form-control" placeholder="Enter patient name" value=<?php echo $patient_name?> >
            </div>
            <div class="form-group">
                <label>Date</label>
                <input type="date" name="date" class="form-control" placeholder="Enter date of entry YYYY-MM-DD" value=<?php echo date('Y-m-d')?>>
            </div>
            <div class="form-group">
                <label>Time</label>
                <input type="text" name="time" class="form-control" placeholder="Enter time of entry hh:mm (24hr format)" value=<?php echo date('H:i')?>>
            </div>
            <div class="form-group">
                <label>Entry type</label>
                <select id = "entryType" class="form-control" name="type" onchange="showOrHideAvailableRooms()">

     				 <option value="routine check-up">routine check-up</option>
     				 <option value="emergency">Emergency</option>
     				 <option value="OPD"> OPD</option>
   				 </select>
            </div>
            <div class="form-group">
                <label>Doctor Specialization</label>
                <select class="form-control" name="specialization">
                	<?php
                		 while($row = $specialization->fetch_assoc()){
 							echo "<option>" . $row['specialization'] . "</option>";
 						}
                	?>
     		
   				 </select>
            </div>

            <div class="form-group" id = 'chooseRoom'>
                <label>Choose Room</label>
                <select class="form-control" name="room">
                	<?php
                		 while($row = $availableRooms->fetch_assoc()){
                		 	$room_no = $row['room_no'];
                		 	$room_type = $row['type'];
 							echo "<option value=\"$room_no\"> Room no:  $room_no, Room type: $room_type </option>";
 						}
                	?>
     		
   				 </select>
            </div>
        	
             <div class="form-group" id = 'noOfDays'>
                <label>Number of stay days </label>
                <input type="number" name="noofdays" class="form-control" placeholder="Enter number of days the patient will stay in room" >
            </div>
        
        </div>
        <div class="modal-footer">
            <button type="submit" name="submitbtn" class="btn btn-primary">Next</button>
        </div>
      </form>

</div>
</div>
</div>



 <?php
include('includes/footer.php');
?>