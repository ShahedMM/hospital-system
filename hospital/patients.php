 <?php
include('includes/header.php'); 
include('includes/navbar.php'); 
include('includes/DBconnection.php');
?>


<?php

$sql = "SELECT * FROM patient";
$result = $conn->query($sql);

?>


<div class="container-fluid">
   <div class="card shadow mb-4">
  <div class="card-header py-3">
    <h4 class="m-0 font-weight-bold text-primary">Patient Data 
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addpatient">
              Add new patient
            </button>
    </h4>
  </div>

  <div class="card-body">

    <div class="table-responsive">

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    
        <thead style="background-color: #cdd0d8; color: white;font-size: 20px">
          <tr>
            <th> Patient Number </th>
            <th> Name </th>
            <th> Date of Birth </th>
            <th>Contact Number</th>
            <th> </th>
             <th> </th>
            <th> </th>
          </tr>
        </thead>

        <tbody style="color: black; font-size: 20px">
          <?php
             while($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["patient_no"] ."</td>";
                echo "<td>" . $row["name"] . "</td>";
                echo "<td>" . $row["date_of_birth"] . "</td>";
                echo "<td>" . $row["contact_no"] . "</td>";
                echo  "<td> <a    href = \"addEntry.php?patient_no=" .$row["patient_no"]. "\" class=\"btn btn-primary\"> Add Entry </a> </td>";
                echo  "<td> <button  type=\"submit\"  class=\"btn btn-primary\"> View History</button> </td>";
                echo  "<td> <a    href = \"deletePatient.php?patient_no=" .$row["patient_no"]. "\" class=\"btn btn-danger\"> Delete </a> </td>";
              }
          ?>

        </tbody>
      </table>

    </div>
  </div>
</div>

</div>

<div class="modal fade" id="addpatient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Patient Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="addPatient.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label> Name </label>
                <input type="text" name="name" class="form-control" placeholder="Enter patient name">
            </div>
            <div class="form-group">
                <label>Date of Birth</label>
                <input type="text" name="dob" class="form-control" placeholder="Enter date of birth as YYYY-MM-DD">
            </div>
            <div class="form-group">
                <label>Contact Number</label>
                <input type="number" name="contact_no" class="form-control" placeholder="Enter Contact Number">
            </div>
        
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="submitbtn" class="btn btn-primary">Add Patient</button>
        </div>
      </form>

    </div>
  </div>
</div>




 <?php
include('includes/footer.php');
?>