 <?php
include('includes/header.php'); 
include('includes/navbar.php'); 
?>

<?php
$conn = mysqli_connect("localhost","admin","admin","hospitaldb");
if ($conn->connect_error) {
    echo "Connection failed: " . $conn->connect_error;
}
$patient_no = $_POST['patient_no'];
$type = $_POST['type'];
$date = date('Y-m-d',strtotime($_POST['date']));
$day =  date('l',strtotime($_POST['date']));
$time = date('H:m',strtotime($_POST['time']));
$specialization = $_POST['specialization'];
$room = $_POST['room'];
$noOfDays = $_POST['noofdays'];

$sql = "SELECT DISTINCT specialization FROM doctors";
$allSpecialization = $conn->query($sql);

$sql = "SELECT * FROM `rooms` WHERE status = 0";
$availableRooms = $conn->query($sql);


$sql = "SELECT start_time,end_time,name,e.ID FROM employee e INNER JOIN doctoravailability d WHERE e.ID = d.ID AND d.day = '$day' AND d.start_time <=  '$time' AND d.end_time >= '$time'  AND e.ID IN (SELECT ID from doctors where specialization = '$specialization') ";

$result = $conn->query($sql);


$doctor_names = array();
 while($row = $result->fetch_assoc()){
 	//$start_time = date('H:m',strtotime($row['start_time']));
 	//$end_time =  date('H:m',strtotime($row['end_time']));
 	//if ($start_time <= $time && $end_time >= $time ) {
 		array_push($doctor_names,$row);
 	//}
 }
?>


<div class="container-fluid">
   <div class="card shadow mb-4">
  <div class="card-header py-3">
    <h4 class="m-0 font-weight-bold text-primary">Choose Doctor </h4>
  </div>

  <div class="card-body">
<form action="submitEntry.php" method="POST">

        <div class="modal-body">
        
                <input style="display: none" type="text" name="patient_no" class="form-control" placeholder="Enter patient name" value=<?php echo $patient_no?> >
           
       
            <div   style="display: none" class="form-group">
                <label>Date</label>
                <input type="date" name="date" class="form-control" placeholder="Enter date of entry YYYY-MM-DD" value=<?php echo $date ?>>
            </div>
            <div  style="display: none" class="form-group">
                <label>Time</label>
                <input type="text" name="time" class="form-control" placeholder="Enter time of entry hh:mm (24hr format)" value=<?php echo $time ?>>
            </div>
            <div  style="display: none" class="form-group">
                <label>Entry type</label>
                <select  class="form-control" name="type" >
     				 <option value="routine check-up" <?php if ($type == 'routine check-up') echo ' selected="selected"'; ?>>routine check-up</option>
     				 <option value="emergency" <?php if ($type == 'emergency') echo ' selected="selected"'; ?>>Emergency</option>
     				 <option value="OPD" <?php if ($type == 'OPD') echo ' selected="selected"'; ?>> OPD</option>
   				 </select>
            </div>


            <div  style="display: none" class="form-group" id = 'chooseRoom'>
                <label>Choose Room</label>
                <select class="form-control" name="room" value=<?php echo $room ?>>
                	<?php
                		 while($row = $availableRooms->fetch_assoc()){
                		 	$room_no = $row['room_no'];
                		 	$room_type = $row['type'];
                		 	if($room == $room_no){
								echo "<option value=\"$room_no\" selected='selected' > Room no:  $room_no, Room type: $room_type </option>";
                		 	}else{
 							echo "<option value=\"$room_no\" > Room no:  $room_no, Room type: $room_type </option>";
 							}
 						}
                	?>
   				 </select>
            </div>
        	
             <div  style="display: none" class="form-group" id = 'noOfDays'>
                <label>Number of stay days </label>
                <input type="number" name="noofdays" class="form-control" placeholder="Enter number of days the patient will stay in room"  value=<?php echo $noOfDays ?>>
            </div>

              <div  style="font-size: 20px; " class="form-group" >
                <label >Choose a doctor from available doctors. <span style="font-weight: bold">The doctors are filtered out based on their specialization, and the date and time of entry.</span>  </label>
                <select required class="form-control" name="doctor" style="font-size: 20px; " >
                	<?php
                	foreach ($doctor_names as $value ) {
                		$ID = $value['ID'];
                		$name = $value['name'];
                		
                		echo "<option value=\" $ID \"> $name </option>";
                	}
                		 
                	?>
     		
   				 </select>

   				 <?php
   				 	if(count($doctor_names) < 1){
   				 		echo "<h5 style='font-weight: bold'> Sorry, there are no available doctors! </h5>";
   				 	}
   				 ?>
            </div>
        
        </div>
        <div class="modal-footer">
        	<?php
            $url = "addEntry.php?patient_no=" .$patient_no;
            
            echo "<a class='btn btn-primary' href='$url'> Go Back</a>";
            ?>
            <button type="submit" name="submitbtn" class="btn btn-primary">Next</button>

        </div>
       
		</form>

</div>
</div>
</div>




 <?php
include('includes/footer.php');
?>